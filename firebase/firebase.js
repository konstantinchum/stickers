import * as firebase from 'firebase/app'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyB8FHoRlvY21MtFVZAOMCc64nqbZ8ncWsE',
  authDomain: 'stickerland-smartfox.firebaseapp.com',
  databaseURL: 'https://stickerland-smartfox.firebaseio.com',
  projectId: 'stickerland-smartfox',
  storageBucket: 'stickerland-smartfox.appspot.com',
  messagingSenderId: '859007535138',
  appId: '1:859007535138:web:ee27bc0f90b9113590a27b',
  measurementId: 'G-ZME82ZFZPQ',
}

if (!firebase.app.length) firebase.initializeApp(firebaseConfig)

export default firebase
