export default {
  header: {
    search: {
      placeholder: 'Search',
    },
    nav: {
      application: 'Application',
      addSet: 'Add set',
    },
  },
  footer: {
    nav: {
      whatsNew: 'What’s new',
      privacyPolicy: 'Privacy Policy',
      addSet: 'Add set',
      application: 'IOS / Android App',
      blog: 'Blog',
      report: 'Report',
      termsAndConditions: 'Terms and Conditions',
    },
  },
  addSetForm: {
    label: {
      name: 'Author name',
      site: 'Site',
      link: 'Link to set',
    },
    button: 'Add',
  },
  featuredSets: {
    title: 'Editor’s Choice',
  },
  featuredCard: {
    watchAll: 'Watch set',
  },
  moreCategories: {
    title: 'Categories',
  },
  moreSets: {
    new: 'New',
    trending: 'Trending',
    animated: 'Animated',
  },
  moreLink: 'Show all',
  addTo: 'Add to:',
  setPageHead: {
    title: 'stickers for WhatsApp, Telegram — Android, iPhone iOS',
    description: [
      'Download and spice up your conversation with',
      'stickers for WhatsApp, Telegram — Android, iPhone iOS',
    ],
  },
}
