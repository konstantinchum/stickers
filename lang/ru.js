export default {
  header: {
    search: {
      placeholder: 'Поиск',
    },
    nav: {
      application: 'Приложение',
      addSet: 'Добавить набор',
    },
  },
  footer: {
    nav: {
      whatsNew: 'Что нового',
      privacyPolicy: 'Политика конфиденциальности',
      addSet: 'Добавить набор',
      application: 'Приложение iOS / Android',
      blog: 'Блог',
      report: 'Жалоба',
      termsAndConditions: 'Правила и условия',
    },
  },
  addSetForm: {
    label: {
      name: 'Имя автора',
      site: 'Сайт автора',
      link: 'Ссылка на набор',
    },
    button: 'Добавить',
  },
  featuredSets: {
    title: 'Выбор редакции',
  },
  featuredCard: {
    watchAll: 'Посмотреть набор',
  },
  moreCategories: {
    title: 'Категории',
  },
  moreSets: {
    new: 'Новые',
    trending: 'Лучшие',
    animated: 'Анимированные',
  },
  moreLink: 'Показать все',
  addTo: 'Добавить в:',
  setPageHead: {
    title: 'стикеров для WhatsApp, Telegram — Android, iPhone iOS',
    description: [
      'Загрузите и оживите свою беседу с помощью стикеров',
      'для WhatsApp, Telegram — Android, iPhone iOS',
    ],
  },
}
