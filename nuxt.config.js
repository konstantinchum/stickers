const firebaseConfig = {
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  projectId: '',
  storageBucket: '',
  messagingSenderId: '',
  appId: '',
  measurementId: '',
}

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Stickerland',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/styles/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~/plugins/loaders.js', ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    [
      '@nuxtjs/eslint-module',
      {
        fix: true,
      },
    ],
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://firebase.nuxtjs.org/
    '@nuxtjs/firebase',
    // https://github.com/nuxt-community/svg-sprite-module
    '@nuxtjs/svg-sprite',
    // https://i18n.nuxtjs.org/
    'nuxt-i18n',
    [
      '@nuxtjs/google-adsense',
      {
        id: 'ca-pub-8167364174021296',
      },
    ],
    ['@nuxtjs/google-tag-manager', { id: 'GTM-W4MWDLW' }],
    // https://sitemap.nuxtjs.org
    '@nuxtjs/sitemap',
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // https://firebase.nuxtjs.org/guide/options
  firebase: {
    config: firebaseConfig,
    services: {
      auth: true,
      firestore: true,
      storage: true,
      analytics: true,
    },
    onFirebaseHosting: true,
  },

  // https://github.com/nuxt-community/svg-sprite-module
  svgSprite: {},

  // https://i18n.nuxtjs.org/
  i18n: {
    lazy: true,
    langDir: 'lang/',
    locales: [
      { name: 'En', code: 'en', iso: 'en-US', file: 'en.js' },
      { name: 'Ру', code: 'ru', iso: 'ru-RU', file: 'ru.js' },
    ],
    defaultLocale: 'en',
    strategy: 'prefix_except_default',
    detectBrowserLanguage: {
      useCookie: true,
      onlyOnRoot: true,
    },
  },

  // https://sitemap.nuxtjs.org/guide/configuration
  sitemap: {
    hostname: process.env.BASE_URL || 'https://stickerland.app',
    gzip: true,
    i18n: {
      locales: ['en', 'ru'],
      routesNameSeparator: '___',
    },
    exclude: ['/admin', '/admin/**'],
    routes: async () => {
      const { default: firebase } = await import('firebase/app')
      await import('firebase/firestore')
      if (!firebase.apps.length) firebase.initializeApp(firebaseConfig)
      const fireStore = firebase.firestore()

      const categories = await fireStore.collection('categories').get()

      const sets = await fireStore
        .collection('sets')
        .where('enabled', '==', true)
        .get()

      const animatedSets = await fireStore
        .collection('animated')
        .where('enabled', '==', true)
        .get()

      const routesFromSnapshot = (path, snapshot) => {
        const locales = ['en', 'ru']
        const defaultLocale = 'en'

        const localeDocs = snapshot.docs.flatMap((doc) =>
          locales.map((locale) => ({ name: doc.id, locale }))
        )

        const localeRoute = (locale, name) =>
          locale === defaultLocale
            ? `${path}/${name}`
            : `/${locale}${path}/${name}`

        return localeDocs.map((doc) => ({
          url: localeRoute(doc.locale, doc.name),
          links: [
            ...locales.map((locale) => ({
              lang: locale,
              url: localeRoute(locale, doc.name),
            })),
          ],
        }))
      }

      return [
        ...routesFromSnapshot('/categories', categories),
        ...routesFromSnapshot('/set', sets),
        ...routesFromSnapshot('/set/animated', animatedSets),
      ]
    },
  },

  // https://nuxtjs.org/docs/2.x/directory-structure/nuxt-config#runtimeconfig
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL || 'https://stickerland.app',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
