import { firestoreAction, vuexfireMutations } from 'vuexfire'

export const state = () => ({
  categories: [],
  featured: [],
  new: [],
  animated: [],
  trending: [],
  sets: [],
  set: {},
  stickers: {},
  popupAddSticker: false,
  sideMenu: false,
})

export const mutations = {
  ...vuexfireMutations,

  newSets(state, payload) {
    state.sets = [...payload]
  },

  popupAddStickerState(state, popupState) {
    state.popupAddSticker = popupState
  },

  sideMenuState(state, menuState) {
    state.sideMenu = menuState
  },
}

export const actions = {
  bindCategories: firestoreAction(async function ({ bindFirestoreRef }) {
    const categories = this.$fire.firestore.collection('categories')
    await bindFirestoreRef('categories', categories)
  }),

  unbindCategories: firestoreAction(function (
    { unbindFirestoreRef },
    promotion
  ) {
    unbindFirestoreRef('categories', false)
  }),

  bindByPromotion: firestoreAction(async function (
    { bindFirestoreRef },
    options
  ) {
    const { bindTo, promotion, limit } = options
    const locale = this.$i18n.locale
    const sets = this.$fire.firestore
      .collection('sets')
      .where('enabled', '==', true)
      .where('promotion', '==', promotion)
      .where('locale', 'in', ['general', locale])
      .orderBy('dataCreatedAt', 'desc')
      .limit(limit)
    await bindFirestoreRef(bindTo, sets)
  }),

  unbindByPromotion: firestoreAction(function (
    { unbindFirestoreRef },
    promotion
  ) {
    unbindFirestoreRef(promotion, false)
  }),

  bindByCategory: firestoreAction(async function (
    { bindFirestoreRef },
    options
  ) {
    const { category, limit } = options
    const locale = this.$i18n.locale
    const categoriesRef = await this.$fire.firestore
      .collection('categories')
      .doc(category)
    const sets = this.$fire.firestore
      .collection('sets')
      .where('enabled', '==', true)
      .where('categoriesDoc', 'array-contains', categoriesRef)
      .where('locale', 'in', ['general', locale])
      .orderBy('dataCreatedAt', 'desc')
      .limit(limit)
    await bindFirestoreRef('sets', sets)
  }),

  bindAnimatedSets: firestoreAction(async function (
    { bindFirestoreRef },
    limit
  ) {
    const locale = this.$i18n.locale
    const animatedSets = this.$fire.firestore
      .collection('animated')
      .where('enabled', '==', true)
      .where('locale', 'in', ['general', locale])
      .orderBy('dataCreatedAt', 'desc')
      .limit(limit)
    await bindFirestoreRef('animated', animatedSets)
  }),

  bindSet: firestoreAction(async function ({ bindFirestoreRef }, options) {
    const { collection, id } = options
    const set = this.$fire.firestore.collection(collection).doc(id)
    await bindFirestoreRef('set', set, { wait: true })
  }),

  bindStickers: firestoreAction(async function ({ bindFirestoreRef }, options) {
    const { collection, id } = options
    const stickers = this.$fire.firestore
      .collection(collection)
      .doc(id)
      .collection('stickers')
    await bindFirestoreRef('stickers', stickers)
  }),

  setPremium: firestoreAction(function ({ state }, options) {
    const { collection, value } = options
    this.$fire.firestore
      .collection(collection)
      .doc(state.set.id)
      .update('premium', value)
  }),

  setPromotion: firestoreAction(function ({ state }, options) {
    const { collection, value } = options
    this.$fire.firestore
      .collection(collection)
      .doc(state.set.id)
      .update('promotion', value)
  }),

  setLocale: firestoreAction(async function ({ state }, options) {
    const { collection, value } = options
    await this.$fire.firestore
      .collection(collection)
      .doc(state.set.id)
      .update('locale', value)
  }),

  scheduleFeaturing: firestoreAction(async function (
    { bindFirestoreRef },
    options
  ) {
    const { collection, id } = options
    const addHours = (date, hours) => {
      const result = new Date(date)
      result.setTime(result.getTime() + hours * 60 * 60 * 1000)
      return result
    }
    const { Timestamp } = this.$fireModule.firestore
    const dateNow = new Date()
    const scheduleSetsQuery = this.$fire.firestore
      .collection(collection)
      .orderBy('featureAt', 'desc')
      .limit(1)
    const scheduledSets = await scheduleSetsQuery.get()
    const scheduledSet = scheduledSets.docs[0].data()
    const { featureAt } = scheduledSet
    let newFeatureAt
    if (featureAt === undefined) {
      newFeatureAt = Timestamp.fromDate(addHours(dateNow, 24))
    } else {
      newFeatureAt = Timestamp.fromDate(addHours(featureAt.toDate(), 24))
    }
    await this.$fire.firestore
      .collection(collection)
      .doc(id)
      .update('featureAt', newFeatureAt)
  }),

  removeStickerPack: firestoreAction(async function ({ state }, options) {
    const { collection, id } = options
    await this.$fire.firestore.collection(collection).doc(id).delete()
  }),

  search: firestoreAction(async function (
    { commit, unbindFirestoreRef },
    searchValue
  ) {
    unbindFirestoreRef('sets')
    const url = new URL(
      'https://us-central1-stickerland-smartfox.cloudfunctions.net/stickerSearch'
    )
    url.search = new URLSearchParams({
      query: searchValue,
      limit: '40',
      from: '0',
    })
    const response = await fetch(url).then((res) => res.json())
    const arr = []
    for (const item of response.hits) {
      arr.push({
        id: item._id,
        ...item._source,
      })
    }
    commit('newSets', arr)
    return response
  }),

  setCategory: firestoreAction(async function ({ state }, options) {
    const { collection, selected } = options
    const set = this.$fire.firestore.collection(collection).doc(state.set.id)
    const categories = this.$fire.firestore.collection('categories')
    const actualCategories = selected.map((doc) => categories.doc(doc.id))
    await set.update('categoriesDoc', actualCategories)
  }),

  async addSet({ state }, formData) {
    const url = new URL(
      'https://us-central1-stickerland-smartfox.cloudfunctions.net/requestSticker'
    )
    url.search = new URLSearchParams({
      author: formData.author,
      website: formData.website,
      url: formData.url,
    })
    return await fetch(url)
  },
}
